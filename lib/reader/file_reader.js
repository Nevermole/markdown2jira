import fs from 'fs'

export default function read (filename) {
    return fs.readFileSync(filename, {encoding: 'utf8'})
}

